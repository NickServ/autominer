# autominer
Automatic ore miner and collector for the Meta Construct server. Not very high code quality, but it works for the most part.
## How to use
Put the autominer.lua file in your garrysmod/lua folder. Join the Meta Construct server and put `lua_openscript_cl autominer.lua` (or whatever name you gave it) in your console.
An interface should pop up with options and instructions for the bot. Type `autominer` in console to open the menu again, and type `autominer_quit` to unload the script.
**Make sure to set the buyer EntIndex!** It may be different between server restarts or server updates. Follow the instructions in the window on how to get the latest EntIndex.
## Bannable?
This script won't get you banned. It's perfectly allowed on the server and devs don't care if you use it (although players might get upset at you stealing their ores!)