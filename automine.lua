--[[
To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty. You should
have received a copy of the CC0 Public Domain Dedication along with this
software. If not, see http://creativecommons.org/publicdomain/zero/1.0/.
]]

--[[
	Autominer (for use on Meta Construct servers)
	Version 3.4
	
	This script works, but it's buggy and shittily written.
	If you can improve it, please do!
	
	Changelog
		Version 1
			+ Initial release.
		Version 2
			+ The bot will now detect if you are dead and try to revive you.
			+ There is now help text in the main window.
			= The panic message when trying to open the menu after vgui_cleanup is now clearer.
			* You will no longer be stuck attacking or moving after quitting.
		Version 3
			+ Will teleport back to mine when idle to make sure we're in bounds.
			- No longer shows target entity class on HUD.
			= Increased maximum range for attempting to mine.
			= Help text now prints to console.
			* Fixed the autominer command.
			* Fixed a sentence in the help text.
		Version 3.1
			= Will no longer advertise if not enabled.
			= Changed advert text slightly.
			* Will now stop advertising if you quit the script.
			* You will no longer be stuck attacking or moving after quitting. (Again)
		Version 3.2
			+ Idiot check: Won't run if already running
		Version 3.3
			* Use print instead of MsgC for the help text.
		Version 3.4
			+ Will now make you equip the crowbar and teleport you to the mine upon enabling.
			= Verbose setting now only affects debug prints (and got rid of the ugly print detour)
			= Debug prints in the set_* functions only occur now if it's not being set to the same value as before
			= Changed some text and added more tooltips.
			= Changed default and minimum window size
			- Removed check for if there's someone in front of us. Didn't stop us from hitting them anyway.
			- No longer print help text to console.
]]

if AUTOMINER then
	print("You can only run one instance of this script at a time! Remember, it's \"autominer\" to bring up the menu again and \"autominer_quit\" to unload the script.")
	return
end

AUTOMINER = true

local window = vgui.Create("DFrame")
window:SetTitle("Autominer")
window:SetDeleteOnClose(false)
window:SetScreenLock(true)
window:SetMinHeight(360/2)
window:SetMinWidth(360*0.7)
window:SetSizable(true)
window:SetIcon("icon16/script.png")
window:SetSize(640, 360)
window:Center()
window:MakePopup()

local panel = vgui.Create("DPanel", window)
panel:DockMargin(0, 3, 0, 1)
panel:DockPadding(3, 3, 3, 3)
panel:Dock(FILL)

local quit = function()
	timer.Destroy("Automine")
	timer.Destroy("Automine_ad")
	hook.Remove("Think", "Automine")
	hook.Remove("HUDPaint", "Automine")
	RunConsoleCommand("-attack")
	RunConsoleCommand("-forward")
	concommand.Remove("autominer")
	concommand.Remove("autominer_quit")
	if window then
		window:SetDeleteOnClose(true)
		window:Close()
	end
	AUTOMINER = false
	print("The autominer has been unloaded! If you want to use it again, reload the script")
end

local quit_button = vgui.Create("DButton", panel)
quit_button:SetTall(30)
quit_button:SetText("Unload")
quit_button:SetTooltip("Click to stop mining and unload the script. You will need to reload the script to start mining again.")
quit_button:Dock(TOP)
quit_button.DoClick = quit

concommand.Add("autominer", function()
	if window then
		window:SetSizable(true)
		window:Center()
		window:MakePopup()
		window:SetVisible(true) -- why do i need this
	else
		print("PANIC! The window could not be found and the autominer will now unload. Did you do vgui_cleanup?")
		quit()
	end
end)

concommand.Add("autominer_quit", quit)

local enabled = false
local verbose = false
local draw_bounding_boxes = true
local advertise = false

local buyer = 1931 -- USUALLY the same, not guaranteed
local area_min = Vector(7509, 4030, -15904)
local area_max = Vector(12248, -3355, -15084)

local me = LocalPlayer()

local classes = {
	mining_ore = true,
	mining_rock = true
}

local set_noclip = function(bool)
	local noclip = me:GetMoveType() == MOVETYPE_NOCLIP
	if bool ~= noclip then
		if verbose then print("Setting noclip to "..tostring(bool)) end
		RunConsoleCommand("noclip")
		noclip = bool
	end
end
local attacking = false
local set_attacking = function(bool)
	--local trace = util.QuickTrace(me:GetShootPos(), me:GetAngles():Forward()*100, me)
	--if trace.Entity and trace.Entity:IsPlayer() then
	--	if verbose then print("Player in the way! Refusing to attack") end
	--	bool = false
	--end
	if bool ~= attacking then
		if verbose then print("Setting attacking to "..tostring(bool)) end
		if bool then
			RunConsoleCommand("+attack")
		else
			RunConsoleCommand("-attack")
		end
		attacking = bool
	end
end
local moving = false
local set_moving = function(bool)
	if bool ~= moving then
		if verbose then print("Setting moving to "..tostring(bool)) end
		if bool then
			RunConsoleCommand("+forward")
		else
			RunConsoleCommand("-forward")
		end
		moving = bool
	end
end
local mode

local ad_text = "GOT  AN  AUTOMINER?    "
local ad_text_len = #ad_text
local ad_pos = 1
if rtchat and rtchat.StartChat and rtchat.ChatTextChanged then
	timer.Create("Automine_ad", 0.5, 0, function()
		if enabled and advertise and not me:IsTyping() then
			rtchat.StartChat()
			rtchat.ChatTextChanged(
				string.sub(ad_text, ad_text_len-ad_pos, ad_text_len)..
				string.sub(ad_text, 0, ad_text_len-ad_pos)
			)
			ad_pos = ad_pos-1
			if ad_pos < 1 then
				ad_pos = ad_text_len
			end
		end
	end)
end

timer.Create("Automine", 300, 0, function()
	if not enabled then
		print("Not enabled, not auto-selling")
		return
	end
	print("Beginning auto-sell")
	enabled = false
	set_attacking(false)
	set_moving(false)
	set_noclip(false)
	mode = "Selling"
	RunConsoleCommand("aowl", "revive")
	timer.Simple(0.5, function()
		RunConsoleCommand("aowl", "goto", buyer)
	end)
	timer.Simple(1.0, function()
		RunConsoleCommand("+use")
	end)
	timer.Simple(1.5, function()
		RunConsoleCommand("-use")
	end)
	timer.Simple(2.0, function()
		RunConsoleCommand("aowl", "goto", "mine")
	end)
	timer.Simple(2.5, function()
		enabled = true
	end)
end)
local candidate
local candidate_pos
local candidate_class
local distance
local teleported_back = false
hook.Add("Think", "Automine", function()
	if enabled then
		if not me:Alive() then
			RunConsoleCommand("aowl", "revive")
			timer.Simple(1, function()
				input.SelectWeapon(me:GetWeapon("weapon_crowbar"))
			end)
			return
		end
		
		local distance_from_candidate
		local user_pos = me:GetShootPos()
		for _, ent in pairs(ents.FindInBox(area_min, area_max)) do
			local found = false
			if not IsValid(ent) then continue end
			--if ent == me then continue end
			if ent:IsPlayer() then continue end
			if not classes[ent:GetClass()] then continue end
			
			local ent_pos
			local minimum, maximum = ent:GetModelBounds()
			ent_pos = LerpVector(0.5, minimum, maximum)
			ent_pos:Rotate(ent:GetAngles())
			ent_pos:Add(ent:GetPos())
			
			local distance_from_ent = user_pos:DistToSqr(ent_pos)
			if distance_from_candidate and distance_from_ent > distance_from_candidate then
				continue
			end
			candidate = ent
			candidate_pos = ent_pos
			candidate_class = ent:GetClass()
			distance_from_candidate = distance_from_ent
			distance = distance_from_candidate
		end
		if candidate_pos then
			teleported_back = false
			distance = user_pos:DistToSqr(candidate_pos)
			local near = distance < 300
			if near and not IsValid(candidate) then
				if verbose then print("We've arrived at the rock but it seems to have vanished; ignoring it (we're probably just out of bounds)") end
				candidate_pos = nil
				return
			end
			if candidate_class == "mining_rock" then
				set_moving(true)
				if near then
					mode = "Mining rock"
					set_attacking(true)
					set_noclip(false)
				else
					mode = "Going to rock"
					set_attacking(false)
					set_noclip(true)
				end
			elseif candidate_class == "mining_ore" then
				mode = "Collecting ore"
				set_attacking(false)
				set_moving(true)
				set_noclip(true)
			else
				mode = "!?!?"
			end
			me:SetEyeAngles( (candidate_pos - user_pos):Angle() )
		else
			if not teleported_back then
				mode = "Idle (No ores found)"
				set_attacking(false)
				set_moving(false)
				set_noclip(false)
				RunConsoleCommand("aowl", "goto", "mine")
				teleported_back = true
			end
		end
	end
end)
local movetype = {
	"Isometric",
	"Walk",
	"Step",
	"Fly (No gravity)",
	"Fly (Gravity)",
	"Physics",
	"Push",
	"Noclip",
	"Ladder",
	"Spectator",
	"Custom"
}
movetype[0] = "None"
hook.Add("HUDPaint", "Automine", function()
	local text = {
		enabled and "Enabled" or "Disabled",
		mode,
		--candidate_class or "nil",
		movetype[me:GetMoveType()],
		distance and math.floor(distance) or "nil"
	}
	for i=1, #text do
		local font
		if i==1 then
			font = "TargetID"
		else
			font = "TargetIDSmall"
		end
		draw.SimpleTextOutlined(
			text[i],
			font,
			ScrW()/2,
			ScrH()/2+12*i,
			Color(255, 255, 255, 255),
			TEXT_ALIGN_CENTER,
			TEXT_ALIGN_TOP,
			0,
			Color(0, 0, 0, 255)
		)
	end
	if draw_bounding_boxes then
		cam.Start3D()
			render.DrawWireframeBox(Vector(), Angle(), area_min, area_max, Color(255, 255, 255, 63))
			
			if IsValid(candidate) then
				local minimum, maximum = candidate:GetModelBounds()
				render.DrawWireframeBox(candidate:GetPos(), candidate:GetAngles(), minimum, maximum)
			end
		cam.End3D()
	end
end)
if panel then
	local enabled_checkbox = vgui.Create("DCheckBoxLabel", panel)
	enabled_checkbox:SetValue(enabled)
	enabled_checkbox:SetText("Enabled")
	enabled_checkbox:SetTooltip("When enabled, the bot will mine ores. You will not be able to look around or move while the bot is mining. Uncheck to stop.")
	enabled_checkbox:SetDark(1)
	enabled_checkbox.OnChange = function(_, state)
		enabled = state
		teleported_back = false
		if enabled then
			input.SelectWeapon(me:GetWeapon("weapon_crowbar"))
			RunConsoleCommand("aowl", "goto", "mine")
		else
			set_attacking(false)
			set_moving(false)
			set_noclip(false)
		end
	end
	enabled_checkbox:DockMargin(0, 3, 0, 0)
	enabled_checkbox:Dock(TOP)
	
	local verbose_checkbox = vgui.Create("DCheckBoxLabel", panel)
	verbose_checkbox:SetValue(verbose)
	verbose_checkbox:SetText("Extra verbose")
	verbose_checkbox:SetTooltip("Prints additional debug information to the console.")
	verbose_checkbox:SetDark(1)
	verbose_checkbox.OnChange = function(_, state)
		verbose = state
	end
	verbose_checkbox:DockMargin(0, 3, 0, 0)
	verbose_checkbox:Dock(TOP)
	
	local boxes_checkbox = vgui.Create("DCheckBoxLabel", panel)
	boxes_checkbox:SetValue(draw_bounding_boxes)
	boxes_checkbox:SetText("Draw bounding boxes")
	boxes_checkbox:SetTooltip("Draws bounding boxes around the mine and the currently targeted ore.")
	boxes_checkbox:SetDark(1)
	boxes_checkbox.OnChange = function(_, state)
		draw_bounding_boxes = state
	end
	boxes_checkbox:DockMargin(0, 3, 0, 0)
	boxes_checkbox:Dock(TOP)
	
	local ad_checkbox = vgui.Create("DCheckBoxLabel", panel)
	ad_checkbox:SetValue(advertise)
	ad_checkbox:SetText("Advertise")
	ad_checkbox:SetTooltip('Displays "'..ad_text..'" above your head in the form of an rtchat marquee. Won\'t display when typing in chat.')
	ad_checkbox:SetDark(1)
	ad_checkbox.OnChange = function(_, state)
		advertise = state
	end
	ad_checkbox:DockMargin(0, 3, 0, 0)
	ad_checkbox:Dock(TOP)
	
	local buyer_cluster = vgui.Create("DPanel", panel)
	buyer_cluster:SetPaintBackground(false)
	buyer_cluster:Dock(TOP)
	
	local buyer_entry = vgui.Create("DNumberWang", buyer_cluster)
	buyer_entry:SetTooltip("The EntIndex of the buyer NPC. Needed to auto-sell.")
	buyer_entry:SetMin(1)
	buyer_entry:SetMax(32000)
	buyer_entry:SetValue(buyer)
	buyer_entry.OnValueChanged = function(_, value)
		print("Changed buyer index to ", value)
		buyer = tonumber(value)
	end
	buyer_entry:Dock(LEFT)
	
	local buyer_label = vgui.Create("DLabel", buyer_cluster)
	buyer_label:SetDark(1)
	buyer_label:SetText("EntIndex of buyer NPC")
	buyer_label:Dock(3, 3, 3, 0)
	buyer_label:Dock(FILL)
	
	local help_text = vgui.Create("RichText", panel)
	help_text:Dock(0, 3, 0, 0)
	help_text:Dock(FILL)
	
	local text = {
		false,
		"Welcome to my autominer. You must follow my instructions to use this bot effectively.",
		"• Go to the woman who collects the ore, look directly at her and type the following in chat:",
		true,
		"\t/lm Say(this:EntIndex())",
		false,
		"You will say a number in the chat. Put that number exactly as it is into the 'Buyer EntIndex' box. It may change if the server restarts or you reconnect.",
		"• Enter the mine and equip your crowbar. Make sure you're in god mode. When you have done these two things, check the 'Enabled' box.",
		"• You may use the console and chat while the bot is running, but you should not attempt to change weapons, look around, or move. If you want to do something else, uncheck the 'Enabled' box.",
		"• When you are done with the autominer, click the 'Unload' button. You will need to reload the script to use the autominer again.",
		"• To bring up this menu again, type 'autominer' in the console or say '/cmd autominer' in chat."
	}
	for i=1, #text do
		if text[i] == true then
			help_text:InsertColorChange(255, 0, 0, 255)
		elseif text[i] == false then
			help_text:InsertColorChange(0, 0, 0, 255)
		else
			--print(text[i])
			help_text:AppendText(text[i].."\n")
		end
	end
end

print("Autominer initialized!")
